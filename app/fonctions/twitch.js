module.exports.isTwitchURL = function (plateformeUrl) {
    try {
        if (plateformeUrl === "www.twitch.tv" || plateformeUrl === "twitch.tv") { // ne s'éxécute que si la plateforme est twitch
            return true
        }
    } catch (error) {
        return false
    }
    return false
}

module.exports.addTwitchFollows = async function (value, msg, knex, modele, fetch, headers) {

    const idDiscord = msg['channel']['guild']['id']
    let idStreams
    const isKnown = await knex("Streams").select('*').where('value', '=', value)
    if (isKnown.length === 0) { //si il n'est pas connu en Base de données
        const resultIsValidID = await fetch(`https://api.twitch.tv/kraken/channels/${value}`, { method: 'GET', headers })
        const isValidID = await resultIsValidID.json()
        if (isValidID['error'] === "Not Found") { // si le profil n'existe pas 
            return 0
        }
        else { // si il existe, alors le rajouter à la base de données
            const resultIdStreams = await knex("Streams").insert({ value, Plateforme: "twitch", diffuse: 0 }).returning('idStreams')
            idStreams = resultIdStreams[0]
        }
    }
    else {
        idStreams = isKnown[0]['idStreams']

    }
    const resultLink = await modele.isAlreadyLinked(idDiscord, idStreams, knex)
    if (resultLink) {
        return 1
    }
    else {
        modele.linkStreamsIDwithDiscordID(value, idDiscord, msg, knex, 'twitch')
        return 2
    }
}