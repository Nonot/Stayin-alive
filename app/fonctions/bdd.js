const paramsDB = require('../config.js').database
const knex = require('knex')({
    client: 'mysql',
    connection: {
        host: paramsDB['host'],
        user: paramsDB['user'],
        password: paramsDB['password'],
        database: paramsDB['database']
    }
});
module.exports = knex 