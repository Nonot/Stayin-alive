module.exports.linkStreamsIDwithDiscordID = async function (value, idDiscord, msg, knex, plateforme) {
    const idDiffusion = msg['channel']['id']
    const isKnownDiscord = await this.isDiscordInDB(idDiscord, knex)
    if (!isKnownDiscord) {
        await addDiscordinDB(idDiscord, idDiffusion)
    }
    const resultIdStreams = await this.findIDStreamsByValue(value, plateforme, knex)
    const idStreams = resultIdStreams['idStreams']
    await knex("Utilise").insert({ idDiscord, idStreams })
}

module.exports.isAlreadyLinked = async function (idDiscord, idStreams, knex) {
    const result = await knex("Utilise").select('*').where('idDiscord', '=', idDiscord).andWhere('idStreams', '=', idStreams)
    if (result.length === 0) {
        return false
    }
    else {
        return true
    }
}

module.exports.isDiscordInDB = async function (idDiscord, knex) {
    const result = await knex("Discord").select('*').where('idDiscord', '=', idDiscord)
    if (result.length === 0) {
        return false
    }
    return true
}

module.exports.findIDStreamsByValue = async function (value, plateforme, knex) {
    const result = await knex("Streams").select('idStreams').where('value', '=', value).andWhere('plateforme', '=', plateforme)
    return result[0]
}
module.exports.addDiscordinDB = async function (idDiscord, idDiffusion, knex) {
    await knex("Discord").insert({ idDiscord, idDiffusion, activation: 1 })
}

module.exports.getListIdDiscord = async function (knex) {
    return await knex('Discord').select('idDiscord')
}

module.exports.removeLink = async function (idStreams, idDiscord, knex) {
    await knex('Utilise').whereRaw(`idDiscord = ${idDiscord} and idStreams = ${idStreams}`).del()
}