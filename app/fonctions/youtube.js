module.exports.isYoutubeURL = function (plateformeUrl) {
    try {
        if (plateformeUrl === "www.youtube.com" || plateformeUrl === "youtube.com") { // ne s'éxécute que si la plateforme est Youtube
            return true
        }
    } catch (error) {
        return false
    }
    return false
}

module.exports.addYoutubeFollows = async function (value, msg, knex, modele, fetch, ytKey) {
    const idDiscord = msg['channel']['guild']['id']
    let idStreams
    const isKnown = await knex("Streams").select('*').where('value', '=', value)
    if (isKnown.length === 0) { //si il n'est pas connu en Base de données
        let resultIsValidID = await fetch(`https://www.googleapis.com/youtube/v3/channels?part=id&id=${value}&key=${ytKey}`)
        let isValidID = await resultIsValidID.json()
        if (isValidID['items'].length === 0) { // si le profil n'existe pas 
            resultIsValidID = await fetch(`https://www.googleapis.com/youtube/v3/channels?part=id&forUsername=${value}&key=${ytKey}`)
            isValidID = await resultIsValidID.json()
            if (isValidID['items'].length === 0) {
                return 0
            }
            else {
                value = isValidID['items'][0]['id']
            }
        }
        // si il existe, alors le rajouter à la base de données
        const resultIdStreams = await knex("Streams").insert({ value, Plateforme: "youtube", diffuse: 0 }).returning('idStreams')
        idStreams = resultIdStreams[0]
    }
    else {
        idStreams = isKnown[0]['idStreams']
    }
    const resultLink = await modele.isAlreadyLinked(idDiscord, idStreams, knex)
    if (resultLink) {
        return 1
    }
    else {
        modele.linkStreamsIDwithDiscordID(value, idDiscord, msg, knex, 'youtube')
        return 2
    }
}
