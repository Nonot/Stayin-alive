'use strict';
//is streaming ? 
// https://api.twitch.tv/helix/streams?user_login=krayn_live
// empty object means no

//is a channel exists ? 
// https://api.twitch.tv/kraken/channels/nonot2
// no = error 404

//how to know if yt channel exists ?
// https://www.googleapis.com/youtube/v3/channels?part=id&id=${channelI}d&key=${ytKey}

// how to know if yt channel is currently streaming ?
// requestUrl = `https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=${channelId}&type=video&eventType=live&key=${ytKey}`
const { Reset, Bright, Dim, Underscore, Blink, Reverse, Hidden, FgBlack, FgRed, FgGreen, FgYellow, FgBlue, FgMagenta, FgCyan, FgWhite, BgBlack, BgRed, BgGreen, BgYellow, BgBlue, BgMagenta, BgCyan, BgWhite } = require('./colors.js')
let paramsDB = {}, YOUTUBE_KEY, TWITCH_CLIENT_ID, DISCORD_TOKEN, CMD_PREFIX

//#################################
// set params from config.js or process.env.
//#################################
if (typeof process.env.DB_HOST !== "undefined") {
    const { DB_HOST, DB_USER, DB_NAME, DB_PWD } = process.env
    paramsDB = {
        ...paramsDB,
        DB_HOST,
        DB_USER,
        DB_NAME,
        DB_PWD,
    }
    TWITCH_CLIENT_ID = process.env.TWITCH_CLIENT_ID
    YOUTUBE_KEY = process.env.YT_KEY
    DISCORD_TOKEN = process.env.DISCORD_TOKEN
}

if (typeof DISCORD_TOKEN === "undefined") {
    console.log("Discord token not set, exit !")
    process.exit(1)
}
if (typeof paramsDB.DB_HOST === "undefined") {
    console.log("Database host not set, exit !")
    process.exit(1)
}
if (typeof paramsDB.DB_USER === "undefined") {
    console.log("Database user not set, exit !")
    process.exit(2)
}
if (typeof paramsDB.DB_PWD === "undefined") {
    console.log("Database password not set, exit !")
    process.exit(3)
}
if (typeof paramsDB.DB_NAME === "undefined") {
    console.log("Database name not set, exit !")
    process.exit(4)
}
const commandPrefix = CMD_PREFIX ? CMD_PREFIX : "+"

const fetch = require("node-fetch");
const R = require("ramda");

const youtubeFonctions = require("./fonctions/youtube.js")
const twitch = require("./fonctions/twitch.js")
const modele = require("./fonctions/modele.js")

let knex = require('knex')({
    client: 'mysql',
    connection: {
        host: paramsDB.DB_HOST,
        user: paramsDB.DB_USER,
        password: paramsDB.DB_PWD,
        database: paramsDB.DB_NAME
    },
});
const headers = { 'CLIENT-ID': TWITCH_CLIENT_ID }

const Eris = require("eris");
const bot = new Eris(DISCORD_TOKEN);

const FIVEMINUTES = 1000 * 60 * 5;
const DISCORD = "Discord"
const STREAMS = "Streams"
const DATABASE_CONNECTION_FAILED = "Connexion à la base de données échouée!"
const HELP_CMD_STATUS = { name: "+help" }
const CHECK_LIVE_STATUS = { name: "update streams infos" }
const IS_BROKEN_STATUS = { name: "a tout cassé" }
// These are the commands the bot knows (defined below):
const knownCommands = { add, remove, togglemode, comehere, list, help, current }
const privilegedCommands = { add, remove, togglemode, comehere }

const requiredPrivileges = ['administrator', 'manageGuild', 'banMembers', 'kickMembers', 'manageRoles', 'manageChannels', 'manageEmojis']


initProject()

async function main() {

    let listeStreams
    try {
        console.log("purge des streams abandonnés");
        knex.raw('DELETE from Streams where idStreams not in ( Select distinct idStreams from Utilise )')
        console.log("lancement des vérifications");
        bot.editStatus("idle", CHECK_LIVE_STATUS)
        listeStreams = await knex(STREAMS).select('*')
    } catch (error) {
        console.log(error);
        console.log(FgRed, DATABASE_CONNECTION_FAILED);
        bot.editStatus("dnd", IS_BROKEN_STATUS)
        return
    }

    let streamTwitchDB = new Map()
    let streamYoutubeDB = new Map()
    let streamTwitch = {}
    let urlQueryTwitch = "https://api.twitch.tv/helix/streams?"
    for (const stream of listeStreams) {
        const value = stream.value
        if (stream.plateforme === "twitch") {
            streamTwitchDB.set(value, { diffuse: stream.diffuse, idStreams: stream.idStreams })
            urlQueryTwitch += `user_login=${stream.value}&`
        }
        else if (stream.plateforme === "youtube") {
            streamYoutubeDB.set(value, { diffuse: stream.diffuse, idStreams: stream.idStreams })
        }
    }

    const responseResultTwitch = await fetch(urlQueryTwitch, { method: 'GET', headers })
    const resultTwitch = await responseResultTwitch.json()
    if (resultTwitch.error === "Unauthorized") {
        console.log("clé twitch invalide, erreur 401");
        console.log("clé actuelle ");
    };

    for (const stream of resultTwitch.data) {
        let user_login = stream.thumbnail_url.replace(/.*(live_user_)+/g, "").replace(/-{width}.*/g, "")
        streamTwitch[user_login] = { diffuse: 1, title: stream.title, thumbnail_url: stream.thumbnail_url.replace(/{width}/, 320).replace(/{height}/, 180) + "?rand=" + Math.random() }
    }

    for (let [key, value] of streamTwitchDB.entries()) {
        const idStreams = value.idStreams
        if (key in streamTwitch) {
            if (value.diffuse === 0) {
                await knex(STREAMS).where('idStreams', '=', idStreams).update({ diffuse: 1 })
                const listeChannels = await knex({ d: 'Discord', u: 'Utilise', s: 'Streams' }).select('d.idDiffusion').where('s.idStreams', '=', idStreams).andWhere('activation', '=', 1).andWhere('u.idStreams', knex.raw('??', ['s.idStreams'])).andWhere('u.idDiscord', knex.raw('??', ['d.idDiscord']))

                let annonce = {
                    "content": `Un live de **${key}** vient de commencer !!!`,
                    "embed": {
                        "author": {
                            "name": key,
                            "url": `https://www.twitch.tv/${key}`,
                            "icon_url": "https://static-cdn.jtvnw.net/jtv_user_pictures/dd542e0da09855b6-profile_image-70x70.jpeg"
                        },
                        "title": streamTwitch[key]['title'],
                        "url": `https://www.twitch.tv/${key}`,
                        "image": {
                            "url": streamTwitch[key]['thumbnail_url']
                        },
                        "color": 6441140,
                        "footer": {
                            "text": "Rejoins-les !"
                        }
                    }
                }
                for (const channel of listeChannels) {
                    await sendMessage(channel['idDiffusion'], annonce, "salon", false, true);
                }
            }
        }
        else {
            if (value.diffuse === 1) {
                await knex(STREAMS).where('idStreams', '=', idStreams).update({ diffuse: 0 })
            }
        }
    }

    for (let [key, value] of streamYoutubeDB.entries()) {
        const responseResultYoutube = await fetch(`https://www.googleapis.com/youtube/v3/search?channelId=${key}&eventType=live&type=video&part=snippet&key=${YOUTUBE_KEY}`, { method: 'GET', headers })
        const resultYoutube = await responseResultYoutube.json()
        const idStreams = value.idStreams
        if (resultYoutube.items.length !== 0) {
            if (value.diffuse === 0) {
                const video = resultYoutube.items[0]
                console.log(video);
                await knex(STREAMS).where('idStreams', '=', idStreams).update({ diffuse: 1 })
                const listeChannels = await knex({ d: 'Discord', u: 'Utilise', s: 'Streams' }).select('d.idDiffusion').where('s.idStreams', '=', idStreams).andWhere('activation', '=', 1).andWhere('u.idStreams', knex.raw('??', ['s.idStreams'])).andWhere('u.idDiscord', knex.raw('??', ['d.idDiscord']))
                let annonce = {
                    "content": `Un live de **${video.snippet.channelTitle}** vient de commencer !!!`,
                    "embed": {
                        "author": {
                            "name": video.snippet.channelTitle,
                            "url": `https://www.youtube.com/watch?v=${key}`,
                            "icon_url": "https://s.ytimg.com/yts/img/favicon_96-vflW9Ec0w.png"
                        },
                        "title": video.snippet.title,
                        "url": `https://www.youtube.com/watch?v=${key}`,
                        "image": {
                            "url": video.snippet.thumbnails.medium.url,
                        },
                        "color": 6441140,
                        "footer": {
                            "text": "Rejoins-les !"
                        }
                    }
                }
                for (const channel of listeChannels) {
                    await sendMessage(channel['idDiffusion'], annonce, "salon", false, true);
                }
            }
        }
        else {
            if (value.diffuse === 1) {
                await knex(STREAMS).where('idStreams', '=', idStreams).update({ diffuse: 0 })
            }
        }
    }

    bot.editStatus("online", HELP_CMD_STATUS)
}

async function initProject() {
    console.log(FgMagenta, "Création des tables si non existante");
    try {
        await knex.raw(`CREATE TABLE IF NOT EXISTs \`Discord\` (
            \`idDiscord\` varchar(18) NOT NULL,
            \`idDiffusion\` varchar(18) NOT NULL,
            \`activation\` tinyint(1) NOT NULL DEFAULT '0',
            PRIMARY KEY (\`idDiscord\`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4`)
        await knex.raw(`CREATE TABLE IF NOT EXISTS \`Streams\` (
            \`idStreams\` int(11) NOT NULL AUTO_INCREMENT,
            \`value\` varchar(50) NOT NULL,
            \`plateforme\` varchar(7) NOT NULL,
            \`diffuse\` tinyint(1) DEFAULT '0',
            PRIMARY KEY (\`idStreams\`)
          ) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4`)
        await knex.raw(`CREATE TABLE IF NOT EXISTS \`Utilise\` (
            \`idDiscord\` varchar(18) NOT NULL,
            \`idStreams\` int(11) NOT NULL,
            PRIMARY KEY (\`idDiscord\`,\`idStreams\`),
            KEY \`idStreams\` (\`idStreams\`),
            CONSTRAINT \`Utilise_ibfk_2\` FOREIGN KEY (\`idStreams\`) REFERENCES \`Streams\` (\`idStreams\`) ON DELETE CASCADE ON UPDATE CASCADE,
            CONSTRAINT \`Utilise_ibfk_3\` FOREIGN KEY (\`idDiscord\`) REFERENCES \`Discord\` (\`idDiscord\`) ON DELETE CASCADE ON UPDATE CASCADE
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4`)
    } catch (error) {
        console.log(FgMagenta, "Création des tables échouée");
        console.log(FgMagenta, error);
        console.log(FgRed, DATABASE_CONNECTION_FAILED)
    }
}

function getTextChannels(idDiscord) {
    const channelsMap = bot.guilds.get(idDiscord).channels.entries()
    const channels = Array.from(channelsMap)
    const textChannels = []
    for (const [key, channel] of channels) {
        if (channel instanceof Eris.TextChannel)
        // on vérifie que le channel est bien de type TextChannel https://github.com/abalabahaha/eris/blob/master/index.js#L34
        {
            textChannels.push(key)
        }
    }
    return textChannels
}

async function deactivateDiscord(id) {
    await knex(DISCORD).where('idDiscord', '=', id).update({ activation: 0 })
}

async function insertDiscord(idDiscord) {
    const firstTextChannel = getTextChannels(idDiscord)[0]
    await knex("Discord").insert({ idDiscord, idDiffusion: firstTextChannel, activation: 1 })
}

async function checkDB() {
    console.log("vérification de l'intégrité de la BDD");
    let serveursConnus
    let serveursActifs
    try {
        serveursConnus = await knex('Discord').select('idDiscord').map(({ idDiscord }) => idDiscord)
        serveursActifs = await knex('Discord').select('idDiscord').where({ activation: 1 }).map(({ idDiscord }) => idDiscord)
    } catch (error) {
        console.log(FgMagenta, error);
        console.log(FgRed, DATABASE_CONNECTION_FAILED)
        return
    }

    const srvConnectés = R.keys(bot.guildShardMap)

    serveursActifs.forEach(srv => {
        if (!R.includes(srv, srvConnectés)) {
            console.log(FgYellow, `${srv} va être désactivé`)
            deactivateDiscord(srv)
        }
    })

    srvConnectés.forEach(idSrv => {
        if (!R.includes(idSrv, serveursConnus)) {
            console.log(FgYellow, `${idSrv} va être ajouté`)
            insertDiscord(idSrv)
        }
    })
    return false
}


setTimeout(() => {
    main()
}, 5000);
setInterval(main, FIVEMINUTES)

async function add(msg, params) {
    if (params.length === 1) {
        const url = params[0]
        const urlDecoded = url.split('/')
        const plateformeUrl = urlDecoded[2]
        if (twitch.isTwitchURL(plateformeUrl) !== false) { // ne s'éxécute que si la plateforme est twitch
            let value = urlDecoded[3]
            switch (await twitch.addTwitchFollows(value, msg, knex, modele, fetch, headers)) {
                case 0:
                    sendMessage(msg.channel.id, "Cet utilisateur ne semble pas exister", "salon", true, true);
                    break;
                case 1:
                    sendMessage(msg.channel.id, "Stream **déjà** sauvegardé", "salon", true, true);
                    break;
                case 2:
                    sendMessage(msg.channel.id, Math.round(Math.random()) ? `${value} a bien été ajouté au fichier "S".` : `${value}, l'étoile montante du twitch game fait maintenant partie de l'élite`, "salon", true, true);
                    break;
            }
        }
        else if (youtubeFonctions.isYoutubeURL(plateformeUrl) !== false) {
            let value = urlDecoded[4]
            switch (await youtubeFonctions.addYoutubeFollows(value, msg, knex, modele, fetch, YOUTUBE_KEY)) {
                case 0:
                    sendMessage(msg.channel.id, "Cet utilisateur ne semble pas exister", "salon", true, true);
                    break;
                case 1:
                    sendMessage(msg.channel.id, "Stream **déjà** sauvegardé", "salon", true, true);
                    break;
                case 2:
                    sendMessage(msg.channel.id, `${value} a bien été ajouté au fichier "S".`, "salon", true, true);
                    break;
            }
        }
        else {
            sendMessage(msg.channel.id, "Plateforme non supporté ou URL invalide", "salon", true, true);
        }
    }
    else {
        sendMessage(msg.channel.id, "Arguments invalides", "salon", true, true);
    }
}

async function remove(msg, params) {
    const idDiscord = msg['channel']['guild']['id']
    if (params.length === 1) {
        const urlDecoded = params[0].split('/')
        const plateformeUrl = urlDecoded[2]
        let value
        let plateforme
        if (twitch.isTwitchURL(plateformeUrl)) {
            value = urlDecoded[3]
            plateforme = 'twitch'
        }
        else if (youtubeFonctions.isYoutubeURL(plateformeUrl)) {
            value = urlDecoded[4]
            plateforme = 'youtube'
        }
        console.log('remove ' + value + ' from ' + plateforme);

        const resultIdStreams = await modele.findIDStreamsByValue(value, plateforme, knex)
        if (resultIdStreams !== undefined) { // ne s'éxécute que si la plateforme est twitch
            const idStreams = resultIdStreams['idStreams']
            await modele.removeLink(idStreams, idDiscord, knex)
            sendMessage(msg.channel.id, `${value} est malheureusement disqualifié suite à une chute dans les escaliers.... Bon rétablissement !`, "salon", true, true);
        }
        else {
            sendMessage(msg.channel.id, "Stream non enregistré", "salon", true, true);
        }
    }
    else {
        sendMessage(msg.channel.id, "Arguments invalides", "salon", true, true);
    }
}

async function togglemode(msg, params) {
    const idDiscord = msg['channel']['guild']['id']
    const idDiffusion = msg['channel']['id']
    const isRegistered = await modele.isDiscordInDB(idDiscord, knex)
    if (!isRegistered) {
        await addDiscordinDB(idDiscord, idDiffusion)
    }
    if (await getActivationMode(idDiscord) === 1) {
        await knex(DISCORD).where('idDiscord', '=', idDiscord).update({ activation: 0 })
        sendMessage(msg.channel.id, "Finalement, je suis un peux fatigué, je vais reposer mon processeur.", "salon", true, true);
    }
    else {
        await knex(DISCORD).where('idDiscord', '=', idDiscord).update({ activation: 1 })
        sendMessage(msg.channel.id, "Je suis fin prêt pour balancer des annonces à gogo !!!", "salon", true, true);
    }
    return
}

async function list(msg, params) {
    const idDiscord = msg['channel']['guild']['id']
    const idDiffusion = msg['channel']['id']
    let message
    const listeStreams = await knex({ s: 'Streams', u: 'Utilise' }).select('s.value', 's.plateforme').where('u.idDiscord', '=', idDiscord).andWhere('s.idStreams', knex.raw('??', ['u.idStreams'])).orderBy('value')

    if (listeStreams.length === 0) {
        message = "Désolé mais vous n'avez pas encore suivi de chaine, utilisez la commande !add suivi de l'url de la chaine à suivre"
        console.log("non");

        sendMessage(msg.channel.id, message, "salon", true, true);
        return
    }
    message = {
        "embed": {
            "title": "Les nominés pour l'oscar du \"meilleur streameur\" sont...",
            "description": "",
            "color": 5017312
        }
    }
    for (const stream of listeStreams) {
        if (stream['plateforme'] === 'twitch') {
            message.embed.description += `https://www.twitch.tv/${stream['value']}\n`
        }
        else if (stream['plateforme'] === 'youtube') {
            message.embed.description += `https://www.youtube.com/channel/${stream['value']}\n`
        }
    }
    console.log(message);

    sendMessage(idDiffusion, message, "salon", true, true);
}
async function comehere(msg, params) {
    const idDiscord = msg['channel']['guild']['id']
    const idDiffusion = msg['channel']['id']
    await knex(DISCORD).where('idDiscord', '=', idDiscord).update({ idDiffusion })
    sendMessage(idDiffusion, "Aussitôt dit, aussitôt fait !", "salon", true, true);
}
async function help(msg, params) {
    const idDiffusion = msg['channel']['id']
    let message = `\n__**===== Aide =====**__`
    message += `\n**+comehere** : vous permet d'indiquer au bot où il devra faire ses annonces`
    message += `\n**+add [url]** : vous permet de suivre une chaine, l'url doit être celle d'une **chaîne** youtube ou twitch`
    message += `\n**+remove [url]** : comme la précédente mais pour arreter de suivre la chaine`
    message += `\n**+list** : vous permet de lister toutes les chaines que vous suivez`
    message += `\n**+current** : liste tous les streams en cours de diffusion`
    message += `\n**+togglemode** : permet de (dés)activer les annonces du bot`
    message += `\n**+help** : vous affiche ce message`
    sendMessage(idDiffusion, message, "salon", true, true);
}
async function current(msg, params) {
    const idDiscord = msg['channel']['guild']['id']
    const idDiffusion = msg['channel']['id']
    let message
    const listeStreams = await knex({ s: 'Streams', u: 'Utilise', d: 'Discord' }).distinct('s.value', 's.plateforme').where('u.idDiscord', '=', idDiscord).andWhere('s.diffuse', '=', 1).andWhere('s.idStreams', knex.raw('??', ['u.idStreams'])).orderBy('value')
    if (listeStreams.length === 0) {
        message = "Il n'y a rien à la télé? ici non plus.... Sors, profites... découvre le monde extérieur !"
        sendMessage(msg.channel.id, message, "salon", true, true);
        return
    }
    message = {
        "embed": {
            "title": "Il n'y a rien à la télé? pas de panic, nous avons des streams de qualité en cours :\n",
            "description": "",
            "color": 5017312
        }
    }
    for (const stream of listeStreams) {
        if (stream['plateforme'] === 'twitch') {
            message.embed.description += `\nhttps://www.twitch.tv/${stream['value']}`
        }
        else if (stream['plateforme'] === 'youtube') {
            message.embed.description += `\nhttps://www.youtube.com/channel/${stream['value']}`
        }
    }
    message.embed.footer = {}
    message.embed.footer.text = `Note : Les vérifications sont relancées toutes les 5 minutes.`
    console.log(message);

    sendMessage(idDiffusion, message, "salon", true, true);
}

async function getActivationMode(idDiscord) {
    const result = await knex(DISCORD).select('activation').where('idDiscord', '=', idDiscord)
    return result[0]['activation']
}

async function sendMessage(idChannel, message, type, addTrashEmote, addRefreshEmote) {
    if (type === "salon") {
        const resultMessage = await bot.createMessage(idChannel, message);
        if (addTrashEmote) {
            await setEmoji(idChannel, resultMessage['id'], "🗑")
        }
        if (addRefreshEmote) {
            await setEmoji(idChannel, resultMessage['id'], "🔄")
        }
    }
}

async function setEmoji(channelID, messageID, reaction) {
    await bot.addMessageReaction(channelID, messageID, reaction)
    return
}

async function joinGuild(idDiscord, idDiffusion, channels) {
    if (await modele.isDiscordInDB(idDiscord, knex) === true) {
        await knex("Discord").update({ idDiscord, idDiffusion, activation: 1 })
    }
    else {
        for (var [key, value] of channels.entries()) {
            if (value['type'] === 0) {
                idDiffusion = value['id']
                await knex("Discord").insert({ idDiscord, idDiffusion, activation: 1 })
                sendMessage(idDiffusion, "Utilise la commande !help pour savoir comment m'utiliser :)", "salon", true, true);
                return
            }
        }
    }
}
async function quitGuild(idDiscord) {
    if (await modele.isDiscordInDB(idDiscord, knex) === true) {
        await knex(DISCORD).where('idDiscord', '=', idDiscord).update({ activation: 0 })
    }
}

async function manageReactionEvent(msg, emoji, user) {
    if (emoji.name === "🗑") {
        const message = await getMessage(msg.channel.id, msg.id)
        if (user !== bot.user.id && message.author.id === bot.user.id) {
            deleteMessage(msg.channel.id, msg.id)
        }
        else if (user !== bot.user.id && user === message.author.id) {
            deleteMessage(msg.channel.id, msg.id)
        }
    }
    else if (emoji.name === "🔄") {
        if (user !== bot.user.id) {
            current(msg)
        }
    }
}
async function deleteMessage(channelID, messageID) {
    await bot.deleteMessage(channelID, messageID)
        .catch(e => {
            // console.log(e);
        })
}

async function getMessage(channelID, messageID) {
    return await bot.getMessage(channelID, messageID)
}
async function isPrivilegedUser(msg) {
    let perms = msg.member.permission
    for (const i of requiredPrivileges) {
        if (perms.has(i)) {
            console.log(`user has ${i} permission`)
            return true
        }
    }
    return false
}
bot.on("ready", () => { // When the bot is ready

    console.log(FgGreen, `${bot.user.username} is connected to Discord's servers !`); // Log it
    checkDB()
    bot.editStatus("online", HELP_CMD_STATUS)
});

bot.on("messageCreate", async (msg) => { // When a message is created
    const message = msg.content
    // console.log(msg);

    // This isn't a command since it has no prefix:
    if (message.substr(0, 1) !== commandPrefix) {
        if (msg.mentions.find(o => o.username === bot.user.username)) {
            msg.addReaction("👋")
        }
        return
    }
    // Split the message into individual words:
    const parse = message.slice(1).split(' ')
    // The command name is the first (0th) one:
    const commandName = parse[0]
    // The rest (if any) are the parameters:
    const params = parse.splice(1)

    const isGranted = await isPrivilegedUser(msg)

    if (commandName in privilegedCommands && !isGranted) {
        const { idDiffusion } = await knex.select('idDiffusion').from('Discord').where({ idDiscord: msg.channel.guild.id }).first()
        sendMessage(idDiffusion, `"Désolé ${msg.member.mention} mais tu n'as pas les droits pour éxécuter cette commande"`, "salon", true, false);
        return
    }
    // If the command is known, let's execute it:
    if (commandName in knownCommands) {
        // Retrieve the function by its name:
        const command = knownCommands[commandName]
        // Then call the command with parameters:
        command(msg, params)
        setEmoji(msg.channel.id, msg.id, "🗑")
        console.log(`* Executed ${commandName} command for ${msg.author.id}`)
    } else {
        console.log(`* Unknown command ${commandName} from ${msg.author.id}`)
    }
});

bot.on("messageReactionAdd", (msg, emoji, user) => {
    manageReactionEvent(msg, emoji, user)
})

bot.on("guildCreate", (guild) => { // quand le bot rejoint un server
    let channels = guild['channels'];
    const idDiscord = guild['id']
    let idDiffusion
    joinGuild(idDiscord, idDiffusion, channels)
});

bot.on("guildDelete", (guild) => {
    const idDiscord = guild['id']
    quitGuild(idDiscord)
});

bot.on('diconnect', () => {
    console.log(FgRed, "Disconnected from Discord :/");
    process.exit()
})

bot.connect(); // Get the bot to connect to Discord