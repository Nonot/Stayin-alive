# Stayin' Alive bot

Stayin' Alive is your self-hosted bot to announce twitch/youtube streams on discord.

## Prerequisites
To make the bot work you'll need these fews things:
 * A Discord bot and its token, you can [make your own](https://discordapp.com/developers/applications/), it is mandatory.
 * A twitch client id, [on the twitch dev panel](https://dev.twitch.tv/)
 * A youtube key, [on the google console](https://developers.google.com/youtube/)

Youtube and twitch key are not mandatory but if they are not filled in, the service will not be able to work !

TODO : disable modules if the key is not valid and prevent user

### OMG i'm not familiar at all with theses sites !
Don't worry for youtube, you can check [this video](https://www.youtube.com/watch?v=3jZ5vnv-LZc), and for twitch, [this one](https://dev.twitch.tv/docs/api/). 
And the most important and easiest, for the discord bot, [this video](https://youtu.be/H3LgNTl761U). Sorry I made the video on a french version of discord but clicks are the same ;)

### Classic install
You now need a server or pc with [npm and Nodejs](https://nodejs.org/en/) installed.

To work the bot will need a database, install a simple Mariadb, create a database and an user with all privileges on it.

Then clone the repo, and go to config section.

### Docker install (recommended)
If you don't want to install npm and node you can use the Docker image.

Then clone the repo and copy the example.db to db

Install [Docker](https://docs.docker.com/install/)
and [Docker-compose](https://docs.docker.com/compose/install/)

## Configure the bot
You are now almost ready.

### Classic
copy the `config.example.js` to `config.js`

### Docker
copy `example.docker-compose.yml` to `docker-compose.yml`

### For all
All you have to do now is to paste the key/id you obtained in [Prerequisites section](https://gitlab.com/Nonot/Stayin-alive#prerequisites) in the appropriate file.

## Launch the bot
### Classic:

`npm start`

### Docker:

`docker-compose up -d`

## Help me
Every PR/help/thanks will be greatly appreciated. :)

If YOU need help to setup the bot do not hesitate to contact me via PM
