FROM node:10.15.0-stretch-slim
WORKDIR /app
COPY package.json /app
COPY package-lock.json /app
RUN npm ci
COPY app /app
CMD npm start
